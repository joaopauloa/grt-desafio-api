package br.ufc.great.api.util;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import br.ufc.great.api.enums.UserSystemPermissions;
import br.ufc.great.api.model.User;
import br.ufc.great.api.repository.UserRepository;

@Component
public class InitializeDatabaseUtil {

	@Autowired
	protected UserRepository userRepository;

	@EventListener(classes = ApplicationReadyEvent.class)
	private void initialize() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		addUserAdmin();
	}

	private void addUserAdmin() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		User admin	= new User();
		admin.setPassword(Utils.cripty("admin"));
		admin.setUserName("Admin");
		admin.setUserEmail("admin@admin.com");
		admin.setPermission(new ArrayList<UserSystemPermissions>(Arrays.asList(UserSystemPermissions.admin_superuser)));
		if (Objects.isNull(userRepository.findByUserEmail(admin.getUserEmail())))
			userRepository.save(admin);
	}

}
