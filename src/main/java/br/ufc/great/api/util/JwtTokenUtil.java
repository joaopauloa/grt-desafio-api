package br.ufc.great.api.util;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.ufc.great.api.model.UserDetailsJwt;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


@Component
public class JwtTokenUtil implements Serializable {
	private static final long serialVersionUID = -2550185165626007488L;
	public static final long JWT_TOKEN_VALIDITY = 30 * 60;

	@Value("${jwt.secret}")
	private String secret;

	public String getEmailFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject).split("\n")[0];
	}

	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}

	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);

	}

	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

	public Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	public String generateToken(UserDetailsJwt userJwtRequest) {
		Map<String, Object> claims = new HashMap<>();
		claims.put("email", userJwtRequest.getUserEmail());
		claims.put("name", userJwtRequest.getUserName());
		claims.put("authorities", userJwtRequest.getAuthorities());
		return doGenerateToken(claims, userJwtRequest.getUserEmail());
	}

	private String doGenerateToken(Map<String, Object> claims, String subject) {
		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
				.signWith(SignatureAlgorithm.HS512, secret).compact();
	}

	public Boolean validateToken(String token, UserDetailsJwt userJwtRequest) {
		final String email = getEmailFromToken(token);
		return (email.equals(userJwtRequest.getUserEmail()) && !isTokenExpired(token));
	}


}
