package br.ufc.great.api.enums;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;

public enum UserSystemPermissions implements GrantedAuthority {

	admin_superuser("admin_superuser"), commonSystemUser("common_system_user");

	private String name;
	private String attribute;

	private UserSystemPermissions(String name) {
		this.name = name;
		this.attribute = name();

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public static List<String> getValuesNomes() {
		return getValuesNomes();
	}

	public String getAuthority() {
		return this.name();
	}

}
