package br.ufc.great.api.controller;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufc.great.api.exceptions.IncorrectCredentials;
import br.ufc.great.api.model.JwtToken;
import br.ufc.great.api.model.UserDetailsJwt;
import br.ufc.great.api.model.request.dto.JwtRequest;
import br.ufc.great.api.model.response.dto.JwtResponse;
import br.ufc.great.api.service.JwtTokenService;
import br.ufc.great.api.service.JwtUserDetailsService;
import br.ufc.great.api.util.JwtTokenUtil;



@RestController
@CrossOrigin
@RequestMapping("/public/authenticate")
public class JwtAuthenticationController {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
	private JwtTokenService jwtTokenService;

	@PostMapping
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest)
			throws UsernameNotFoundException, NoSuchAlgorithmException, UnsupportedEncodingException,
			IncorrectCredentials {
		final UserDetailsJwt userJwtRequest = userDetailsService.loadUserByEmail(authenticationRequest);
		final String token = jwtTokenUtil.generateToken(userJwtRequest);
		jwtTokenService.save(new JwtToken(token));
		return ResponseEntity.ok(new JwtResponse(token));
	}

}
