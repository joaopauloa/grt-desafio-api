package br.ufc.great.api.controller;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufc.great.api.exceptions.ConflictException;
import br.ufc.great.api.exceptions.NotFoundException;
import br.ufc.great.api.model.User;
import br.ufc.great.api.model.request.dto.UserRequestDTO;
import br.ufc.great.api.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserService userService;
	
	@PreAuthorize("hasAnyAuthority('admin_superuser')")
	@PostMapping
	public void add(@RequestBody UserRequestDTO user)
			throws ConflictException, NoSuchAlgorithmException, UnsupportedEncodingException {
		userService.add(user);
	}
	
	@PreAuthorize("hasAnyAuthority('admin_superuser')")
	@GetMapping(value = "/{page}/{size}")
	public ResponseEntity<Page<User>> getAll(@PathVariable("page")int page, @PathVariable("size") int size) {
		return ResponseEntity.ok(userService.get(page, size));
	}
	
	@PreAuthorize("hasAnyAuthority('admin_superuser')")
	@PutMapping
	public void update(@RequestBody UserRequestDTO userRequestDTO) throws NotFoundException {
		userService.update(userRequestDTO);
	}

	@PreAuthorize("hasAnyAuthority('admin_superuser')")
	@GetMapping(value = "/username/{username}")
	public ResponseEntity<Page<User>> searchByName(@PathVariable("username") String userName) {
		return ResponseEntity.ok(userService.searchByName(userName));
	}

	@PreAuthorize("hasAnyAuthority('admin_superuser')")
	@GetMapping(value = "/cpf/{cpf}")
	public ResponseEntity<Page<User>> searchByCpf(@PathVariable("cpf") String cpf) {
		return ResponseEntity.ok(userService.searchByCpf(cpf));
	}

	@PreAuthorize("hasAnyAuthority('admin_superuser')")
	@GetMapping(value = "/rg/{rg}")
	public ResponseEntity<Page<User>> searchByRg(@PathVariable("rg") String rg) {
		return ResponseEntity.ok(userService.searchByRg(rg));
	}
	
	@PreAuthorize("hasAnyAuthority('admin_superuser')")
	@DeleteMapping(value = "/{userid}")
	public void delete(@PathVariable("userid") long userId ) {
		userService.delete(userId);
		
	}

}
