package br.ufc.great.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.ufc.great.api.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUserEmail(String email);
	User findByUserName(String userName);
	Page<User> findAll(Pageable pageable);
	Page<User> findAllByUserNameContaining(String userName, Pageable pageable);
	Page<User> findAllByCpf(String cpf, Pageable pageable);
	Page<User> findAllByRg(String rg, Pageable pageable);
	

}
