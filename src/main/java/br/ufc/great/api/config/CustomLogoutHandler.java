package br.ufc.great.api.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import br.ufc.great.api.exceptions.NotFoundException;
import br.ufc.great.api.service.JwtTokenService;

@Component
public class CustomLogoutHandler implements LogoutHandler {

	@Autowired
	private JwtTokenService jwtTokenService;

	@Override
	public void logout(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) {
		final String authHeader = request.getHeader("Authorization");
		if (authHeader != null) {
			String tokenValue = authHeader.substring(7);
			try {
				jwtTokenService.revokeToken(tokenValue);
				SecurityContextHolder.clearContext();
			} catch (NotFoundException e) {
				e.printStackTrace();
			}
		}
	}

}
