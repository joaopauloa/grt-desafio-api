package br.ufc.great.api.service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.ufc.great.api.exceptions.ConflictException;
import br.ufc.great.api.exceptions.NotFoundException;
import br.ufc.great.api.model.User;
import br.ufc.great.api.model.request.dto.UserRequestDTO;
import br.ufc.great.api.repository.UserRepository;
import br.ufc.great.api.util.Utils;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public void add(UserRequestDTO userRequest)
			throws ConflictException, NoSuchAlgorithmException, UnsupportedEncodingException {
		User user = userRepository.findByUserEmail(userRequest.getUserEmail());
		if (Objects.isNull(user)) {
			userRequest.setPassword(Utils.cripty(userRequest.getPassword()));
			userRepository.save(new User(userRequest));

		} else
			throw new ConflictException(user);
	}

	public Page<User> get(int pabeNumber, int pageSize) {
		PageRequest pageRequest = new PageRequest(pabeNumber, pageSize);
		Pageable pageable = pageRequest;
		Page<User> pageResult = userRepository.findAll(pageable);

		return pageResult;

	}
	
	public Page<User> searchByName(String userName){
		PageRequest pageRequest = new PageRequest(0, 10);
		Pageable pageable = pageRequest;
		Page<User> pageResult = userRepository.findAllByUserNameContaining(userName, pageable);
		return pageResult;
	}
	
	public Page<User> searchByCpf(String cpf){
		PageRequest pageRequest = new PageRequest(0, 10);
		Pageable pageable = pageRequest;
		Page<User> pageResult = userRepository.findAllByCpf(cpf, pageable);
		return pageResult;
	}
	
	public Page<User> searchByRg(String rg){
		PageRequest pageRequest = new PageRequest(0, 10);
		Pageable pageable = pageRequest;
		Page<User> pageResult = userRepository.findAllByRg(rg, pageable);
		return pageResult;
	}
	

	public void update(UserRequestDTO userRequest) throws NotFoundException {
		User user = userRepository.findOne(userRequest.getUserId());
		if (Objects.nonNull(user)) {
			user.setUserName(userRequest.getUserName());
			user.setBirthdate(userRequest.getBirthdate());
			user.setCpf(userRequest.getCpf());
			user.setMotherName(userRequest.getMotherName());
			user.setRg(userRequest.getRg());
			user.setUserEmail(userRequest.getUserEmail());
			userRepository.save(user);
		} else
			throw new NotFoundException(User.class, String.valueOf(userRequest.getUserId()));
	}
	
	public void delete(long userId) {
		User user = userRepository.findOne(userId);
		if(Objects.nonNull(user)) {
			userRepository.delete(userId);
		}
	}

}
