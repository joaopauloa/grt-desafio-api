package br.ufc.great.api.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufc.great.api.exceptions.NotFoundException;
import br.ufc.great.api.model.JwtToken;
import br.ufc.great.api.repository.JwtTokenRepository;

@Service
public class JwtTokenService {
	@Autowired
	JwtTokenRepository jwtTokenRepository;

	public JwtToken getToken(String tokenValue) {
		return jwtTokenRepository.findByJwtToken(tokenValue);
	}

	public void revokeToken(String tokenValue) throws NotFoundException {
		JwtToken jwtToken = jwtTokenRepository.findByJwtToken(tokenValue);
		if (Objects.isNull(jwtToken)) {
			throw new NotFoundException(JwtToken.class, tokenValue);
		}
		jwtTokenRepository.delete(jwtToken);
	}

	public void save(JwtToken newToken) {
		jwtTokenRepository.save(newToken);
	}

}
