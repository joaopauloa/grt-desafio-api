package br.ufc.great.api.service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.ufc.great.api.exceptions.IncorrectCredentials;
import br.ufc.great.api.model.User;
import br.ufc.great.api.model.UserDetailsJwt;
import br.ufc.great.api.model.request.dto.JwtRequest;
import br.ufc.great.api.repository.UserRepository;
import br.ufc.great.api.util.Utils;



@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	UserRepository userRepository; 

	public UserDetailsJwt loadUserByEmail(JwtRequest authRequest) throws UsernameNotFoundException,
			NoSuchAlgorithmException, UnsupportedEncodingException, IncorrectCredentials {
		UserDetailsJwt userDetailsRequest = loadUserByEmail(authRequest.getEmail());
		if (Objects.equals(userDetailsRequest.getPassword().toLowerCase(),
				Utils.cripty(authRequest.getPassword()).toLowerCase()))
			return userDetailsRequest;
		throw new IncorrectCredentials();
	}

	public UserDetailsJwt loadUserByEmail(String email) throws UsernameNotFoundException {
		User user = userRepository.findByUserEmail(email);

		if (Objects.nonNull(user)) {
			return new UserDetailsJwt(user.getUserId(), user.getUserName(), user.getUserEmail(), user.getPassword(),
					user.getPermission());
		} else {
			throw new UsernameNotFoundException("Email not found" + email);
		}
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return null;
	}
}
