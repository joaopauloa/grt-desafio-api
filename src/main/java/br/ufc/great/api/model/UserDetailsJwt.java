package br.ufc.great.api.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class UserDetailsJwt extends User {
	private static final long serialVersionUID = 2391214499711400826L;
	private Collection<? extends GrantedAuthority> authorities;

	public UserDetailsJwt(Long userId, String username, String email, String password,
			Collection<? extends GrantedAuthority> authorities) {
		this.setUserId(userId);
		this.setUserName(username);
		this.setUserEmail(email);
		this.setPassword(password);
		this.setAuthorities(authorities);
	}

	public UserDetailsJwt(String username, String email, String password, String cpfCnpj, String fullName,
			Collection<? extends GrantedAuthority> authorities) {
		this.setUserName(username);
		this.setUserEmail(email);
		this.setPassword(password);
		this.setAuthorities(authorities);
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
}
