package br.ufc.great.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import br.ufc.great.api.enums.UserSystemPermissions;
import br.ufc.great.api.model.request.dto.UserRequestDTO;

@Entity
@Table(name = "user")
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	long userId;

	private String userName;

	@Column(unique = true)
	private String cpf;

	@Column(unique = true)
	private String rg;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date birthdate;

	private String motherName;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDateTime creationDate;

	@Column(unique = true)
	private String userEmail;

	@JsonIgnore
	private String password;

	private ArrayList<UserSystemPermissions> permission;

	public User() {
	}

	public User(UserRequestDTO userRequestDTO) {
		userName = userRequestDTO.getUserName();
		cpf = userRequestDTO.getCpf();
		rg = userRequestDTO.getRg();
		birthdate = userRequestDTO.getBirthdate();
		motherName = userRequestDTO.getMotherName();
		userEmail = userRequestDTO.getUserEmail();
		password = userRequestDTO.getPassword();
		creationDate = LocalDateTime.now();  
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}


	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ArrayList<UserSystemPermissions> getPermission() {
		return permission;
	}

	public void setPermission(ArrayList<UserSystemPermissions> permission) {
		this.permission = permission;
	}

}
