package br.ufc.great.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ConflictException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConflictException(Object obj) {
		super("Element " + obj.toString() + " already found in database.");
	}

}
