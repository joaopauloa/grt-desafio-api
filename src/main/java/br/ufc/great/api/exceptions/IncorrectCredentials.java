package br.ufc.great.api.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class IncorrectCredentials extends Exception {

	private static final long serialVersionUID = 1L;

	public IncorrectCredentials() {
		super("Incorrect password.");
	}

}
