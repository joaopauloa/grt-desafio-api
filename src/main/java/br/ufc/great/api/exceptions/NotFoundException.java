package br.ufc.great.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public NotFoundException(Class<?> clazz, String campo, String identificador) {
		super("Not found " + clazz.getSimpleName() + " with " + campo + " = " + identificador);
	}

	public NotFoundException(Class<?> clazz, String identificador) {
		super("Not found " + clazz.getSimpleName() + " with value = " + identificador);
	}

}
